/*
 * @file    : config.js
 * @author  : AnujGupta
 * @desc    : configuration file
 */

var environment     = require('./env/env.json');
var localEnv        = require('./env/local.json');

var getConfiguration = function() {
    // setting default to development.
    // NODE_ENV=development node server.js
    // NODE_ENV=production node server.js
    
    var node_env = process.env.NODE_ENV || 'development';
    if(node_env === 'local') {
        return localEnv[node_env];
    } else {
        return environment[node_env];
    }
};

exports.config = getConfiguration();
