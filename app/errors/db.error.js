/*
 * @file: db.error.js
 * @author: Anuj Gupta
 * @desc: provided to define error when there is error in database
 */
var errorDomain = "Database";

exports.ErrorDB = {
    INVALID_FIELD_NAME : {code:301001, message:"Find field name not match", domain: errorDomain},
    DB_CONNECTION_FAILED : {code:301002, message:"unable to connect to MySQL db", domain: errorDomain},
    DELETE_FAILED_MYSQL: {code:301003, message:"unable to delete record", domain: errorDomain},
    INSERT_FAILED_MYSQL: {code:301004, message:"unable to store record", domain: errorDomain},
    DELETE_FAILED_MONGO: {code:301005, message:"unable to delete record", domain: errorDomain},
    INSERT_FAILED_MONGO: {code:301006, message:"unable to store record", domain: errorDomain}
};