/*
 * @file: param.error.js
 * @author: KD
 * @desc: provided to define error when there is error in parameters
 */
var errorDomain = "params";

exports.ErrorParams = {
    SUCCESSFUL_REQUEST : {code:303000, message: "request completed successfully", domain: errorDomain},
    MISSING_MANDATORY_PARAMETERS : {code:303001, message: "missing mandatory parameters", domain: errorDomain},
    INVALID_PARAMETER : {code:303002, message: "one of the parameters provided is invalid", domain: errorDomain},
    INVALID_EMAIL : {code: 303003, message: "Invail email address", domain: errorDomain},
    DUPLICATE_USER_REQUEST : {code:303004, message: "email already exist in database", domain: errorDomain},
    INVALID_OPERATION_REQUESTED: {code: 303005, message: "looks like you have requested invalid api, try again with correct set of apis", domain: errorDomain},
    DUPLICATE_CODE : {code:303006, message: "code already exist in database, please create again", domain: errorDomain},
    DUPLICATE_TOKEN : {code:303007, message: "token already exist in database, please create again", domain: errorDomain},
    DUPLICATE_EDUCATOR : {code:303008, message: "educator already exist, please create new", domain: errorDomain}
};