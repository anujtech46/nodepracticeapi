
/*
 * @file    : profile_channelpartner.route.js
 * @author  : AnujGupta
 * @desc    : route file for channel partner middleware.
 */

var studentProfileController   = require('../../controllers/profiles/profile.student.controller');
var BASE_URL_V1                       = "/api/tutr/v1";

module.exports = function (app) {
    
        app.route(BASE_URL_V1 + '/profile/student')
        .post(studentProfileController.apiC)
        .get(studentProfileController.apiR)
        .put(studentProfileController.apiU)
        .delete(studentProfileController.apiD);

        app.route('/profile/students')
                .get(studentProfileController.getAll);
};