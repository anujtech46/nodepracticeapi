/**
 * @file: profie.channelpartner.controller.js
 * @author: KD
 * @desc: controller file for channel partner profile
 */
var StudentProfile   = require('../../models/profiles/profile.student.model');
var PERROR                  = require('../../errors/param.error.js').ErrorParams;
var DBERROR                 = require('../../errors/db.error.js').ErrorDB;
var LOG                     = require('../logger.js').logger;

/*
 * @name: create
 * @desc: creates user's profile
 * @param: educatorProfile object.
 * @param: callback with error or no error.
 * @return: none
 */
function create(profile, callback) {
    // Save profile now.
    profile.save(function(err) {
        if(err) {
            if(err.code === 11000) { // profile already existing.
                LOG.error("user profile was existing [%s], returning error", err.code);
                return callback(PERROR.INVALID_OPERATION_REQUESTED, null);
            }
            console.log(err);
            LOG.error("unable to save user profile [%s]", err);
            return callback(PERROR.INVALID_OPERATION_REQUESTED, null); 
        }
        else {
            // function completed successfully, indicate caller
            return callback(null, profile);                        
        }
    });
};

/*
 * @name: edit
 * @desc: primarily updates user's profile
 * @param: educatorProfile object
 * @param: callback with error or no error.
 * @return: none
 */
function edit(profile, callback) {
    StudentProfile.findOneAndUpdate({userid: profile.userid}, profile, function(err, updatedProfile){
        if(err) {
            LOG.error("could not found user profile, this is odd", err);
            return callback(PERROR.INVALID_PARAMETER, null);
        }
        else {
            get(profile.userid, function(err, fetchedProfile) {
                if (err) {
                    return callback(PERROR.INVALID_PARAMETER, null);
                }
                else {
                    return callback(null, fetchedProfile);
                }
            });            
        }
    });
};

/*
 * @name: get
 * @desc: function returns the profile object
 * @param: userid, userid of the user
 * @param: callback with error or no error
 * @return: none
 */
function get(userid, callback) {
    StudentProfile.findOne({userid: userid}).populate("commcredential").exec(function(err, res) {
        if(err || !res) {
            LOG.error("could not found user profile, this is odd", null);
            return callback(PERROR.INVALID_PARAMETER, null);
        }
        // object selected from db, now return it to caller
        return callback(null, res);
    });
};

function getAll(req, res) {
    StudentProfile.find({}, function(err, students) {
        if(err || !students) {
            LOG.error("could not found user profile, this is odd", null);
            return res.send(PERROR.INVALID_PARAMETER);
        }
        return res.send(students);
    });
};

/*
 * @name: remove
 * @desc: removes profile
 * @param: userid, student's userid
 * @param: callback, with error or no error
 * @return: none
 */
function remove(userid, callback) {
    StudentProfile.findOne({userid: userid}, function(err, profile) {
        if(err || !profile) {
            LOG.error("could not found user profile, this is odd", err);
            return callback(PERROR.INVALID_PARAMETER, null);
        }

        profile.remove(function(err) {
            if(err) {
                LOG.error("unable to delete user's profile from DB", err);
                return callback(PERROR.INVALID_PARAMETER, null);
            }
            return callback(null, profile);
        });
    });
};

/*
 * @name: apiC
 * @desc: restful wrapper to call create function
 * @param: req object, pull out respective tags from it
 * @param: res object, sending response back to caller
 */

function apiC(req, res) {
    var user = new StudentProfile({
        userid:          req.body.userid,
        firstname:       req.body.firstname,
        lastname:        req.body.lastname,
        qualification:   req.body.qualification,
        institute:       req.body.institute,
        headline:        req.body.headline
    });
    

    create(user, function(err, profile) {
        if(err) {
            res.send(err);
        }
        else {
            LOG.info("profile created");
            profile.status = PERROR.SUCCESSFUL_REQUEST;
            res.send(profile);            
        }

    });
};

function apiR(req, res) {
    
    userid = (req.header('userid'));
    get(userid, function(err, profile) {
        if(err) {
            res.send(err);
        } else {
            profile.status = PERROR.SUCCESSFUL_REQUEST;
            res.send(profile);
        }
    });
}

function apiU(req, res) {
    var user = new StudentProfile({
        userid:          req.userid
    });
    // populating the model based upon the availability
    if(req.body.firstname) {
        user.firstname = req.body.firstname;
    }
    if(req.body.lastname) {
        user.lastname = req.body.lastname;
    }
    if(req.body.qualification) {
        user.qualification = req.body.qualification;
    }
    if(req.body.institute) {
        user.institute = req.body.institute;
    }
    if(req.body.headline) {
        user.headline = req.body.headline;
    }
    

    edit(user, function(err, updatedProfile) {
        if(err) {
            res.send(err);
        }
        updatedProfile.status = PERROR.SUCCESSFUL_REQUEST;
        res.send(updatedProfile);
    });
}

function apiD(req, res) {
    remove(req.userid, function(err, profile){
        if(err) {
            res.send(err);
        } else {
            res.send(PERROR.SUCCESSFUL_REQUEST);
        }
    });
}
/*
 * Exporting functions from modules
 */
module.exports.create       = create;
module.exports.edit         = edit;
module.exports.remove       = remove;
module.exports.get          = get;
module.exports.getAll       = getAll;

module.exports.apiC         = apiC;
module.exports.apiR         = apiR;
module.exports.apiU         = apiU;
module.exports.apiD         = apiD;



