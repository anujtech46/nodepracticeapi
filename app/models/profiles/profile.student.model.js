/**
 * file: profie.student.model.js
 * author: KD
 * desc: model file for channel partner profile
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var StudentProfileSchema = new Schema( {
    userid: {
        type: String,
        unique: true,
        required: true
    },
    firstname:String,
    lastname: String,
    dob: {
        type: Date,
        default: Date.now
    },
    address: String,
    qualification: String,
    institute: String,
    // image will be uploaded to storage and file key should be stored here
    thumbnail: {
        type: String,
        default: 'defaultth.png'
    },
    // image will be uploaded to storage and file key should be stored here
    wallpaper: {
        type: String,
        default: 'defaultwp.png'
    },
    headline: String,
    modifiedon: {
        type: Date,
        default: Date.now
    },
    createdon: Date,
    location: Object,
    onboard: {
        type: String,
        default: 'custom'
    },
    gender: {
        type: String
    }
} );

var StudentProfileSchema = mongoose.model('studentChannelPartner', StudentProfileSchema);


module.exports = StudentProfileSchema;

