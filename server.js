var config                  = require('./config/config.js').config;
var express                 = require('express');
var bodyParser              = require('body-parser');
var methodOverride          = require('method-override');
var session                 = require('express-session');
var mongoose                = require('mongoose');
var https                   = require('https');
var fs                      = require('fs');
var PERROR                  = require('./app/errors/param.error.js').ErrorParams;
var DBERROR                 = require('./app/errors/db.error.js').ErrorDB;
var LOG                     = require('./app/controllers/logger').logger;

var https_options = {
    key: fs.readFileSync('./ssl/trringconnect.com.key', 'utf8'),
    cert: fs.readFileSync('./ssl/63b80a241e7e3e17.crt', 'utf8'),
    ca: [fs.readFileSync('./ssl/gd_bundle-g2-g1.crt', 'utf8')]
};

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(methodOverride()); 

mongoose.connect(config.mongourl);

mongoose.connection.on('connected', function () {
    LOG.info('Mongoose connected to ' + config.mongourl);
});
//use session middleware
app.use(session({
    saveUninitialized : true,
    resave : true,
    secret : config.sessionsecret
}));


app.use(function(req, res, next) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
                res.header('Access-Control-Allow-Headers', 'userid, Content-Type, Authorization, Content-Length, X-Requested-With, *');

                if ('OPTIONS' === req.method) {
                    res.send(200);
                }
                else {
                    next();
                };
            });
app.use(function(err, req, res, next) {
    return res.send(PERROR.INVALID_OPERATION_REQUESTED);
});

app.get('/', function(req, res){
    LOG.info("App connected");
    res.send('Welcome to TutrApi');
});

require('./app/routes/profiles/profile.student.route')(app);

https.createServer(https_options, app).listen(config.appport);
console.log('server running at PORT [%d]', config.appport);
